/*import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;
import java.util.Scanner;
import java.util.Random;*/
import java.io.*;
import java.util.*;

/*
 * This class includes several methods that are answers to the questions found in the document
 */

/* 
 * Below are the answers to the questions that were asked in the document

 * Question 1: Instead of an independent numberOfPlanets variable, where do you think the number of planets should come from? 
 * Answer 1: A field variable inside the class itself. Make sure to pass this through the constructor

 * Question 2: We have an array, but how do we add planets to the array?
 * Answer 2: Use a for loop for initializing more than one object at a time or initialize the objects one at a time.
 
 * Question 3: Can you figure out how to add eight other planets of our solar system to the planets array? 
 * Answer 3: Do the same exact thing: initialize all of the elements from indices: 0-7, since it is one's-indexed
 
 * Question 4: What if the planetary system has more than 8 planets?  We need a better data structure to hold our planet objects.
 * Answer 4: We can use a List (ArrayList in Java instead, as it is a dynamic version of an array and it uses the Java List interface).
 
 * Question 5: The SpaceAdventure constructor is repetitive.  We should refactor.  What does it mean to "refactor" the initializer?  
 * Answer 5: To refactor the initializer means to restructure the initializer in such a way that the external behaviour of the initializer isn’t affected.
             Oftentimes, to refactoring, means to make optimizations to one's code.
 
 * Question 6: Can you think of a way we can use the array of planets to let the traveler specify the planet he or she wishes to travel to?  
 * Answer 6: Use the identifier of each of the Planet elements/objects that are stored inside of the array. That way, the traveler would be informed of which planet he/she is on.
             This would be in the form of a field String variable to store the name of the planet object.

 * Question 7: How can we ask the traveler which planet he or she would like to visit, and then display that planet's description? 
 * Answer 7: We can use a series of if/else statements (otherwise known as control structure) to determine whether or not the traveler wants to visit the specified planet or not. 
             If he/she does, then simply print out the field String variable, as mentioned above.
             For the planet’s description, we will have to simply create another field String variable to store the description of the planet. 
             Repeat the same step to print out the description.
             If the answer is no to the response, then we will display a message prompting the user to enter the name of the planet that he/she would like to visit.

 * Question 8: Discuss how one might print the description of the Planet in the planetarySystem.planets array whose name matches the value of planetName. 
 * Answer 8: One can implement a separate helper method to check if the element exists in the array. 
             If it does, then return the index of the element and then print out the description go the Planet object at that certain index. 
             Else if the element doesn’t exist, return out -1.
             In this case, I ended up just using a for-each loop to loop through the contents of the array to check if the planet name matches the name of the planet object.
             If an element is found, then print out the description of the planet object using the field String variable.

 * Question 9: Discuss the drawbacks of using a long, explicit if statement, such as if planetName == "Mercury". 
 * Answer 9: It’s not only tedious, but It may take a long time to run, if there are a lot of elements inside of the array.  
             This is because we have to exhaustively check if each of the planet names matches the name of the planet object.

 * Question 10: What happens when the traveler types something else besides a valid planet name? 
 * Answer 10: The above code will not print out anything, since the for loop will stop running and no such element is found.

 * Question 11: Why is our program crashing with a runtime error? 
                Can you think of a way we can improve the code, to handle cases where the array of planets is empty? 
 * Answer 11: The reason why the program may be crashing with a runtime error may be due to the fact that the array is empty/null, since it is uninitialized.

 * Question 12: Our codebase has grown, and the SpaceAdventure initializer has a bit of a "code smell." 
                Although it works ok, can you think of ways we can improve the initializer? 
 * Answer 12: We can use an alternative Data Structure instead of an array to efficiently and dynamically store the Planet objects, as mentioned in one of the previous responses. 
              This is considered easier and less tedious since you don’t have to initialize the elements at the given indices - instead, you simply add the objects to the ArrayList.
 */

class Reader {

    /*
     * @ param filename: The name of the text file - make sure that this is correct
     * @ returns A Scanner object that you can use to read the content of the text file
     */

    public Scanner getScanner (String filename) {
        InputStream myFile = getClass().getResourceAsStream(filename);
        if (myFile != null) return new Scanner(myFile);
        return null;
    }
}

class Planet_Info {
    String name, desc;
    Planet_Info (String a, String b) {
        this.name = a;
        this.desc = b;
    }
}

public class A_Space_Adventure {

    public static Scanner sc = new Scanner(System.in);
    public static final int numberOfPlanets = 8;
    public static final double circumference = 24859.82;
    public static String name, response, planet_response;
    public static PlanetarySystem planetarySystem;
    public static Planet[] planets; // Alternatively, we can use an ArrayList instead of an array
    // public static ArrayList<Planet> planets = new ArrayList<Planet>();
    public static Random rng = new Random(); // A random number generator used in determineDestination() method
    public static Planet planet; // A planet
    public static Reader read = new Reader();
    public static String file = "RyanSolarSystem.txt";
    public static Scanner sc1;
    public static ArrayList<String> names = new ArrayList<String>();
    public static ArrayList<String> description = new ArrayList<String>();
    public static ArrayList<Planet_Info> info = new ArrayList<Planet_Info>();
    public static HashMap<String, String> hm = new HashMap<String, String>(); // Store the planets' names along with their descriptions

    public A_Space_Adventure () { // TODO: Must refactor the initializer in order to avoid excessive and repetitive code. We can use a for-loop instead.
        // Use the Reader class to extract the contents of the data text file
        sc1 = read.getScanner(file);
        while (sc1.hasNextLine()) {
            String line = sc1.nextLine();
            String line1 = sc1.nextLine();
            hm.put(line, line1);
        }
        /*
        for (String s : names) {
            for (String a : description) {
                // Check for matching strings
                if (a.startsWith(s)) { // Two strings match
                    if (hm.get(s) == null) hm.put(s, a);
                }
                else continue;
            }
        }
        */
        planets = new Planet[hm.size()];
        int idx = 0;
        for (String key : hm.keySet()) planets[idx++] = new Planet(key, hm.get(key));
        planetarySystem = new PlanetarySystem("Solar System", planets);
        /*
        // Note: The planets are not listed/initialized in order that they appear, starting from the sun
        Planet mercury = new Planet("Mercury", "Mercury is a very hot planet, closest to the sun.");
        // planetarySystem.planets[0] = mercury; // Initialize the first element of the array
        planets[0] = mercury;
        Planet neptune = new Planet("Neptune", "Neptune is a very cold planet, furthest from the sun.");
        // planetarySystem.planets[1] = neptune;
        planets[1] = neptune;
        Planet venus = new Planet("Venus", "Venus has the longest rotation period of any of the planets in the Solar System");
        planets[2] = venus;
        Planet jupiter = new Planet("Jupiter", "Jupiter is considered to be the largest planet of them all.");
        planets[3] = jupiter;
        Planet uranus = new Planet("Uranus", "Uranus has the third-largest planetary radius and the fourth-largest planetary mass in the Solar System.");
        planets[4] = uranus;
        Planet saturn = new Planet("Saturn", "Saturn is the second-largest planet in the Solar System.");
        planets[5] = saturn;
        Planet mars = new Planet("Mars", "Mars is the second-largest planet in the Solar System, right after Mercury.");
        planets[6] = mars;
        Planet earth = new Planet("Earth", "Earth is the planet known to support more life-forms than all of the other planets in the Solar System.");
        planets[7] = earth;
        */
    }

    public static void displayIntroduction () {
        System.out.println("Welcome to the " + planetarySystem.name + "!");
        System.out.println("There are " + planetarySystem.planets.length + " planets to explore.");
    }

    public static String responseToPrompt (String prompt) {
        String response = "";
        System.out.println(prompt);
        response = sc.nextLine();
        return response;
    }

    public static void greetAdventurer () {
        name = responseToPrompt("What is your name?");
        System.out.println("Nice to meet you, " + name+ ". My name is Eric.");
    }

    // Method returns the index of the element in the array if it exists
    // Else it returns -1 if it is not found
    public static int Get_Index (String planetName) {
        for (int i=0; i<planetarySystem.planets.length; i++) {
            if (planetarySystem.planets[i].name.equals(planetName)) return i;
        }
        return -1;
    }

    // This method checks to see if the element is found in the array, before executing
    public static void visit (String planetName) {
        System.out.println("Travelling to " + planetName + "...");
        /*int index = Get_Index(planetName);
        if (index != -1) { // Only execute this if the index exists
        System.out.print("Arrived at " + planetName + ". ");
        System.out.println(planetarySystem.planets[index].description); // Print the description on a separate line
        */
        // We can also use a for-each loop to greatly simplify everything...
        for (Planet next : planetarySystem.planets) {
            if (next.name.equals(planetName)) {
                System.out.println("Arrived at " + planetName + ". " + next.description);
            }
        }
    }

    public static void determineDestination() {
        String decision = "";
        while (!(decision.equals("Y") || decision.equals("N"))) {
            decision = responseToPrompt("Shall I randomly choose a planet for you to visit? (Y or N)");
            if (decision.equals("Y")) {
                System.out.println("Ok, Travelling to...");
                /*
                int upperbound = planetarySystem.planets.length; // Determine the upper_bound/length of the array
                int index = rng.nextInt(upperbound); // Generate a random number within the range: 0-8
                visit(planetarySystem.planets[index].name); // Now visit the randomly-generated planet
                */
                planet = planetarySystem.randomPlanet();
                if (planet == null) System.out.println("Sorry, but there are no planets in this system.");
                else if (planet != null) visit(planet.name);
            } else if (decision.equals("N") ) {
                planet_response = responseToPrompt("Ok, name the planet you would like to visit...");
                visit(planet_response);
            } else {
                System.out.println("Huh? Sorry, I didn't get that.");
            }
        }
    }

    public static void start () {
        displayIntroduction();
        greetAdventurer();
        // Check for empty planets first
        if (!(planetarySystem.planets == null || planetarySystem.planets.length == 0)) {
            System.out.println("Let's go on an adventure!");
            determineDestination();
        }
    }
}
