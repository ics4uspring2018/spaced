import java.io.*;
import java.util.*;

public class PlanetarySystem {

	public static Random rng = new Random();
	public static String name;   // A property of PlanetarySystem
	public static Planet[] planets;
	public PlanetarySystem (String name_, Planet[] planets_) {  // The Constructor (Initializer)
		this.name = name_;
		this.planets = planets_;
	}

	Planet randomPlanet () {
		if (!(planets == null || planets.length == 0)) {
			int index = rng.nextInt(planets.length);
			return planets[index];
		}
		return null; // Else return null if the array is empty or not initialized
	}
}